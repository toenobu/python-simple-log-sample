# -*- coding: utf-8 -*-
"""
    p3
"""

import logging, logging.config

logging.config.fileConfig("logging.ini", disable_existing_loggers=False)
logger = logging.getLogger("p3")


def loop():
    try:
        raise CustomError
    except Exception as e:
        logger.error(e, exc_info=True)


class CustomError(Exception):
    pass
