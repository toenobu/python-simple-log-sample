# -*- coding: utf-8 -*-
"""
    main
"""

import time
import logging

from multiprocessing import Process

from proc import p1, p2, p3

# logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(name)s %(levelname)s:%(message)s')
logging.config.fileConfig("logging.ini", disable_existing_loggers=False)
logger = logging.getLogger(__name__)


def main():
    starttime = time.time()
    ps = []

    def start_process():
        targets = [p1.loop, p2.loop, p3.loop]

        for t in targets:
            p = Process(target=t, args=())

            logger.info(f"start {p.name}")
            p.start()

            ps.append(p)

    def wait_process():
        for p in ps:
            p.join()

    start_process()
    wait_process()

    endtime = time.time()
    duration = endtime - starttime
    logger.info(f"done", extra={"run_duration": duration})


if __name__ == "__main__":
    main()
